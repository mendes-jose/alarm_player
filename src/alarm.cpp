// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2020, José Mendes Filho
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#include "alarm.h"
#include <algorithm> // min max

const uint16_t Alarm::highest_priority = 0;

Alarm::Alarm(uint16_t priority, uint32_t beep_period, uint32_t beep_duration,
             char activation_key, uint32_t intermission_duration,
             uint16_t cycles_preintermission, uint32_t repetitions,
             uint16_t char_duration)
    : priority_(priority)
    , beep_period_(std::max(beep_period, beep_duration))
    , beep_duration_(beep_duration)
    , activation_key_(activation_key)
    , intermission_duration_(std::max(int64_t(0), int64_t(intermission_duration) - beep_period_ + beep_duration_))
    , cycles_preintermission_(cycles_preintermission)
    , repetitions_(repetitions)
    , is_active_(false)
    , activation_time_pt_(std::chrono::steady_clock::now())
    , color_("\033[1;" + std::to_string(91 + priority_ % 6) + "m") // pick color among [RED GREEN YELLOW BLUE MAGENTA CYAN] based on priority
    , nb_cycles_since_act_(0)
    , char_duration_(char_duration)
    , time_since_start_of_intermission_(0)
    , total_intermission_duration_(0)
{
}

std::string Alarm::getSound()
{
    if (!is_active_ || beep_period_ == 0 || beep_duration_ == 0)
    {
        return "_";
    }

    if (act_rising_edge_)
    {
        auto time_since_actv = std::chrono::steady_clock::now() - activation_time_pt_;

        /* Next line ensures we syncronize activation time with getSound caller
         * clock (assuming caller uses the same char duration).
         * The extra 1/50 of char duration delay gives robustness against variance
         * of caller clock */
        activation_time_pt_ -= time_since_actv % char_duration_ + std::chrono::microseconds(int(char_duration_ * 1000 / 50));

        act_rising_edge_ = false;
    }

    auto time_since_actv = std::chrono::steady_clock::now() - activation_time_pt_;
    auto millisecs_since_actv = std::chrono::duration_cast<std::chrono::milliseconds>(time_since_actv).count();

    char char_to_print = '_';
    if (intermission_duration_ == 0 || cycles_preintermission_ == 0) // without intermission
    {
        if (repetitions_ == 0 || nb_cycles_since_act_ < repetitions_)
        {
            nb_cycles_since_act_ = millisecs_since_actv / beep_period_;
            char_to_print = millisecs_since_actv % beep_period_ < beep_duration_ ? 'X' : '_';
        }
    }
    else // with intermission
    {
        if (repetitions_ == 0 || nb_cycles_since_act_ < repetitions_)
        {
            if (nb_cycles_since_act_ != 0 &&
                nb_cycles_since_act_ % cycles_preintermission_ == 0 &&
                time_since_start_of_intermission_ < intermission_duration_)
            {
                char_to_print = '_';
                time_since_start_of_intermission_ += char_duration_;
                total_intermission_duration_ += char_duration_;
            }
            else
            {
                nb_cycles_since_act_ = (millisecs_since_actv - total_intermission_duration_ + char_duration_) / beep_period_; // + total_intermission_duration_/intermission_duration_;
                char_to_print = (millisecs_since_actv - total_intermission_duration_) % beep_period_ < beep_duration_ ? 'X' : '_';

                // reset time_since_start_of_intermission_ after the end of the first complete cycle post intermission
                if ((nb_cycles_since_act_ - 1) % cycles_preintermission_ == 0)
                {
                    time_since_start_of_intermission_ = 0;
                }
            }
        }
    }

    return color_ + char_to_print + "\033[0m";
    // return color_ + char_to_print + "\033[0m" + std::to_string(nb_cycles_since_act_);
}

void Alarm::setActive(bool active)
{
    if (!is_active_ && active)
    {
        act_rising_edge_ = true;
        nb_cycles_since_act_ = 0;
        activation_time_pt_ = std::chrono::steady_clock::now();
        total_intermission_duration_ = 0;
    }

    is_active_ = active;
}
