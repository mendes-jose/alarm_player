// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2020, José Mendes Filho
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#include "alarm_manager.h"
#include <chrono>
#include <thread>

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <algorithm>

AlarmManager::AlarmManager(uint16_t char_duration, std::stringstream* output)
    : char_duration_(char_duration)
    , next_prio_(Alarm::highest_priority)
    , out_(nullptr)
    , ss_(output)
    , stopped_(false)
{

    // attach std::cout's or ss_'s streambuf to out_
    if (output != nullptr)
        out_.rdbuf(ss_->rdbuf());
    else
        out_.rdbuf(std::cout.rdbuf());

    interrupted = false;

    sigIntHandler_.sa_handler = signalHandler_;
    sigemptyset(&sigIntHandler_.sa_mask);
    sigIntHandler_.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler_, NULL);
}

void AlarmManager::add(uint32_t beep_period, uint32_t beep_duration,
    char activation_key, uint32_t intermission_duration,
    uint16_t cycles_preintermission, uint32_t repetitions)
{
    alarms_.push_back(std::make_shared<Alarm>(next_prio_, beep_period, beep_duration,
        activation_key, intermission_duration, cycles_preintermission,
        repetitions, char_duration_));
    next_prio_++;
}

void AlarmManager::add(uint16_t priority, uint32_t beep_period, uint32_t beep_duration,
    char activation_key, uint32_t intermission_duration,
    uint16_t cycles_preintermission, uint32_t repetitions)
{
    alarms_.push_back(std::make_shared<Alarm>(priority, beep_period, beep_duration,
        activation_key, intermission_duration, cycles_preintermission,
        repetitions, char_duration_));

    if (priority >= next_prio_)
    {
        next_prio_ = priority + 1;
    }
}

void AlarmManager::add(AlarmPtr alarm)
{
    alarm->setCharDur(char_duration_); // ensures char duration is consistent among alarms
    alarms_.push_back(alarm);

    if (alarm->getPriority() >= next_prio_)
    {
        next_prio_ = alarm->getPriority() + 1;
    }
}

// Modified version of the _kbhit function from the following website:
// https://www.flipcode.com/archives/_kbhit_for_Linux.shtml
int AlarmManager::kbhit_()
{
    static const int STDIN = 0;
    static bool initialized = false;

    if (!initialized)
    {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        initial_term_settings_ = term;
        term.c_lflag &= (~ICANON & ~ECHO); // ~ECHO: stops echoing what is typed from the keyboard
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytes_waiting = 0;
    ioctl(STDIN, FIONREAD, &bytes_waiting);
    return bytes_waiting;
}

void AlarmManager::processInput_()
{
    char last_typed_char = 0;

    int bytes_waiting = kbhit_();
    while (bytes_waiting > 0)
    {
        auto ret = read(fileno(stdin), &last_typed_char, 1);
        bytes_waiting--;
    }

    bool any_change = false;
    for (auto& alarm : alarms_)
    {
        // Toggle
        if (alarm->getActivationKey() == last_typed_char)
        {
            alarm->setActive(!alarm->isActive());
            any_change = true;
            break;
        }
    }

    // If any alarm got toggled, we sort them again
    if (any_change)
    {
        sort(alarms_.begin(), alarms_.end(), compare_alarms_);
    }
}

void AlarmManager::run()
{
    sort(alarms_.begin(), alarms_.end(), compare_alarms_);

    auto next_time_pt = std::chrono::steady_clock::now();
    auto init_time_pt = next_time_pt;

    // application loop
    while (!interrupted && !stopped_)
    {
        processInput_();

        std::string sound_char_to_print;

        if (!alarms_.empty() && alarms_.front()->isActive())
        {
            sound_char_to_print = alarms_.front()->getSound();
        }
        else
        {
            sound_char_to_print = "_";
        }

        out_ << sound_char_to_print << std::flush;

        next_time_pt += std::chrono::milliseconds(char_duration_);
        std::this_thread::sleep_until(next_time_pt);
    }

    // restore term settings
    tcsetattr(0, TCSANOW, &initial_term_settings_);

    // only print summary if streamming to cout
    if (ss_ == nullptr)
    {
        std::cout << std::endl;

        const std::string FG_B_L_CYAN ("\033[1;96m");
        const std::string RESET ("\033[0m");
        const std::string FG_ITALIC ("\033[3m");

        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(next_time_pt - init_time_pt).count();
        std::cout << std::endl << FG_ITALIC << FG_B_L_CYAN << "Elapsed time: " << elapsed << " ms (" << elapsed / char_duration_ << " characters printed)" << RESET << std::endl;
    }
}
