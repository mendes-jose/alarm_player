# Alarm Player

- [Project proposal (from Wandercraft)](./doc/proposal.md)
- [Implementation thoughts](#implementation-thoughts)
- [API](#api)
- [Requirements](#requirements-for-building-and-running-locally)
- [How-to](#how-to)
- [Improvements (TODO)](#todo)

## Implementation thoughts

### Concurrent code?

There is no need to create a concurrent code to accomplish the desired behavior as defined in the project proposal. There are 3 main reasons for that:
- Only one alarm plays at a given moment
- An alarm that is not playing is basically not doing anything else
- User input can be processed sequentially, inside the same main loop which executes the action of playing the alarms

### Architecture

Two main classes are used: `AlarmManager` and `Alarm`.

`Alarm` provide a generalized way to create alarms. The different information necessary to specify the behavior of each alarm (high, medium and low priority)
is so minimal that a hierarchical organization (with base and derived alarm classes) is unnecessary in my view.

The alarms differ from each other by the following parameters:
- priority
- beep period
- beep duration
- activation key (e.g. 'h', 'm', 'l')
- intermission duration (to account for the 5 beeps followed by 2 secs pause of the high priority alarm)
- number of cycles pre-intermission (needed for the same as the above item)
- number of repetitions (on the proposal all alarms repeat indefinitely, but a limit number of cycles can be useful)

The `Alarm` class is used for the 3 different alarms and could be used to generate extra custom ones. Specialization comes from setting the above parameters and not from the code structure itself.

`AlarmManager` contains all the alarms that can be played and encapsulates the main loop. This loop plays the highest priority activated alarm and process the user input from the keyboard.

### Behavior

- The highest priority activated alarm is the only one played at a given moment. However, the other activated, low priority alarms will continue to "experience the passage of time". This means that if they happen to become the highest priority alarm they behave as if they where playing on the background all the while. Use keyboard keys (`h`, `m` and `l`) to toggle alarms and `ctrl-c` to stop and exit.
- (Re)activation of an alarm resets it. This means that the number of past performed beeping cycles will be set to zero and that a new beeping cycle will start.
- To improve UX a different color is associated with each priority. Colors will be picked in the following order: RED, GREEN, YELLOW, BLUE, MAGENTA and CYAN with RED the highest priority and CYAN the 6th highest priority. Defaut (WHITE) color is used when no alarm is active.

#### Output example

![](./doc/output.png)

## API

### Example 1

The main application doesn't keep a handle on the alarms and alarms' priorities are defined implicitly according to their creation order:

```cpp
#include "alarm_manager.h"

...

AlarmManager am;
                                 // time is in [ms]
am.add(500, 250, 'h', 2000, 5);  // high priority
am.add(1000, 250, 'm');          // medium priority
am.add(30000, 1000, 'l');        // low priority

am.run();                        // play and process user input
```

### Example 2

Priorities are explicitly defined:

```cpp
#include "alarm_manager.h"

...

AlarmManager am;

am.add(Alarm::highest_priority + 3, 60000, 1000, 'v');    // lowest priority (Very low priority)
am.add(Alarm::highest_priority + 2, 30000, 1000, 'l');
am.add(Alarm::highest_priority + 1, 1000, 250, 'm');
am.add(Alarm::highest_priority, 500, 250, 'h', 2000, 5);  // highest priority

am.run();
```

### Example 3

The main application keeps handles on the alarms:

```cpp
#include "alarm_manager.h"

...

AlarmManager am;

auto alarm_high = std::make_shared<Alarm>(Alarm::highest_priority, 500, 250, 'h', 2000, 5);
auto alarm_med = std::make_shared<Alarm>(Alarm::highest_priority + 1, 1000, 250, 'm');
auto alarm_low = std::make_shared<Alarm>(Alarm::highest_priority + 2, 30000, 1000, 'l');

am.add(alarm_high);
am.add(alarm_med);
am.add(alarm_low);

alarm_high->setActive(false);
alarm_med->setActive();        // same as setActive(true)
alarm_low->setActive(false);

am.run();
```

## Requirements for building and running locally

- g++
- make
- cmake
- gtest (for building tests)

## How-to

### Build
```sh
cd alarm_player/build
cmake .. && make
```

### Build with tests and run them
```sh
cd alarm_player/build
cmake -DUSE_GTEST=true .. && make && ./alarm_player-tests
```

### Run the app

```sh
./alarm_player/build/alarm_player
```

Use keys `h`, `m`, `l` to toggle activation of high, medium and low priority alarms respectively.

Use `ctrl-c` to exit and see a short run summary.

### Run the app (with docker)

```sh
docker pull registry.gitlab.com/mendes-jose/alarm_player:master
docker run -it --rm registry.gitlab.com/mendes-jose/alarm_player:master
```

## TODO

- Analyze the the whole user input buffer instead of using only most recent character
- Improve UX by using a text-based, terminal-independent UI (checkout this project's **ncurses branch**)
- Add option to play an actual beep (with speaker-test utility for example)
- Enable/Disable colors option
