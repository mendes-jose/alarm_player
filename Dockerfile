FROM alpine:latest

RUN apk add --no-cache g++ cmake make gtest-dev

COPY . /

RUN cmake -DUSE_GTEST=true . && make

CMD ["./alarm_player"]
