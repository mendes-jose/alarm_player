// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2020, José Mendes Filho
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#include <gtest/gtest.h>
#include <random>
#include <thread>
#include <chrono>
#include "alarm_manager.h"

// to generate random numbers
static std::random_device rd;
static std::mt19937 gen(rd()); // Mersenne Twister pseudo-random generator
static std::uniform_int_distribution<uint16_t> u16_distr(0, std::numeric_limits<uint16_t>::max());
static std::uniform_int_distribution<uint32_t> u32_distr(0, std::numeric_limits<uint32_t>::max());
static std::uniform_int_distribution<char> ch_distr(0, 127);

TEST(AlarmManager, apiExample1)
{
    AlarmManager am;

    am.add(500, 250, 'h', 2000, 5);  // high priority
    am.add(1000, 250, 'm');          // medium priority
    am.add(30000, 1000, 'l');        // low priority

    ASSERT_EQ(am.nbAlarms(), 3);
    ASSERT_EQ(am.nbActiveAlarms(), 0);
}

TEST(AlarmManager, apiExample2)
{
    AlarmManager am;

    am.add(Alarm::highest_priority + 3, 60000, 1000, 'v');    // lowest priority (Very low priority)
    am.add(Alarm::highest_priority + 2, 30000, 1000, 'l');
    am.add(Alarm::highest_priority + 1, 1000, 250, 'm');
    am.add(Alarm::highest_priority, 500, 250, 'h', 2000, 5);  // highest priority

    ASSERT_EQ(am.nbAlarms(), 4);
    ASSERT_EQ(am.nbActiveAlarms(), 0);
}

TEST(AlarmManager, apiExample3)
{
    AlarmManager am;

    auto alarm_high = std::make_shared<Alarm>(Alarm::highest_priority, 500, 250, 'h', 2000, 5);
    auto alarm_med = std::make_shared<Alarm>(Alarm::highest_priority + 1, 1000, 250, 'm');
    auto alarm_low = std::make_shared<Alarm>(Alarm::highest_priority + 2, 30000, 1000, 'l');

    am.add(alarm_high);
    am.add(alarm_med);
    am.add(alarm_low);

    alarm_high->setActive(false);
    alarm_med->setActive();        // same as setActive(true)
    alarm_low->setActive(false);

    ASSERT_EQ(am.nbAlarms(), 3);
    ASSERT_EQ(am.nbActiveAlarms(), 1);
}

TEST(AlarmManager, charDurSync)
{
    const auto char_dur = u16_distr(gen);

    AlarmManager am(char_dur);
    std::vector<AlarmPtr> alarms;

    for (auto i = 0; i < int(1e4); ++i)
    {
        alarms.push_back(std::make_shared<Alarm>(u16_distr(gen), u16_distr(gen),
                 u32_distr(gen), ch_distr(gen), u32_distr(gen), u16_distr(gen),
                 u32_distr(gen), u16_distr(gen)));
        am.add(alarms.back());
    }

    for (const auto& alarm : alarms)
    {
        ASSERT_EQ(alarm->getCharDuration(), char_dur);
    }
}

TEST(AlarmManager, OutputLength_1sec)
{
    const uint16_t wait_dur = 1000;
    const uint16_t char_dur = 250;

    std::stringstream ss;
    AlarmManager am(char_dur, &ss);

    am.add(500, 250, 'h', 2000, 5);  // high priority
    am.add(1000, 250, 'm');          // medium priority
    am.add(30000, 1000, 'l');        // low priority

    auto time_pt = std::chrono::steady_clock::now();

     // play and process user input in a different thread
    std::thread th(&AlarmManager::run, &am);

    time_pt += std::chrono::milliseconds(wait_dur);
    std::this_thread::sleep_until(time_pt);

    am.stop();

    th.join();

    ASSERT_EQ(ss.str().length(), wait_dur/char_dur);
}
