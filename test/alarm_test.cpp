// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2020, José Mendes Filho
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#include <gtest/gtest.h>
#include <random>
#include <thread>
#include "alarm.h"

// to generate random numbers
static std::random_device rd;
static std::mt19937 gen(rd()); // Mersenne Twister pseudo-random generator
static std::uniform_int_distribution<uint16_t> u16_distr(0, std::numeric_limits<uint16_t>::max());
static std::uniform_int_distribution<uint32_t> u32_distr(0, std::numeric_limits<uint32_t>::max());
static std::uniform_int_distribution<char> ch_distr(0, 127);

TEST(Alarm, construcSimple)
{
    const uint16_t prio = Alarm::highest_priority;
    const uint32_t beep_period = 500;
    const uint32_t beep_duration = 200;
    const char     activation_key = 'h';
    const uint32_t intermission_duration = 3000;
    const uint16_t cycles_preintermission = 3000;
    const uint32_t repetitions = 20;
    const uint16_t char_duration = 100;

    const uint32_t true_beep_period = std::max(beep_period, beep_duration);
    const uint32_t true_intermission_duration = std::max(int64_t(0), int64_t(intermission_duration) - true_beep_period + beep_duration);

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key,
             intermission_duration,
             cycles_preintermission,
             repetitions,
             char_duration);

    ASSERT_EQ(al.getPriority(), prio);
    ASSERT_EQ(al.getBeepPeriod(), true_beep_period);
    ASSERT_EQ(al.getBeepDuration(), beep_duration);
    ASSERT_EQ(al.getActivationKey(), activation_key);
    ASSERT_EQ(al.getIntermissionDuration(), true_intermission_duration);
    ASSERT_EQ(al.getCyclesPreintermission(), cycles_preintermission);
    ASSERT_EQ(al.getRepetitions(), repetitions);
    ASSERT_EQ(al.getCharDuration(), char_duration);
}

TEST(Alarm, construcNullAlarm)
{
    Alarm al(0, 0, 0, 0, 0, 0, 0, 0);

    ASSERT_EQ(al.getPriority(), 0);
    ASSERT_EQ(al.getBeepPeriod(), 0);
    ASSERT_EQ(al.getBeepDuration(), 0);
    ASSERT_EQ(al.getActivationKey(), 0);
    ASSERT_EQ(al.getIntermissionDuration(), 0);
    ASSERT_EQ(al.getCyclesPreintermission(), 0);
    ASSERT_EQ(al.getRepetitions(), 0);
    ASSERT_EQ(al.getCharDuration(), 0);
}

TEST(Alarm, construcRandom)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = u32_distr(gen);
    const uint32_t beep_duration = u32_distr(gen);
    const char     activation_key = ch_distr(gen);
    const uint32_t intermission_duration = u32_distr(gen);
    const uint16_t cycles_preintermission = u16_distr(gen);
    const uint32_t repetitions = u32_distr(gen);
    const uint16_t char_duration = u16_distr(gen);

    const uint32_t true_beep_period = std::max(beep_period, beep_duration);
    const uint32_t true_intermission_duration = std::max(int64_t(0), int64_t(intermission_duration) - true_beep_period + beep_duration);

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key,
             intermission_duration,
             cycles_preintermission,
             repetitions,
             char_duration);

    ASSERT_EQ(al.getPriority(), prio);
    ASSERT_EQ(al.getBeepPeriod(), true_beep_period);
    ASSERT_EQ(al.getBeepDuration(), beep_duration);
    ASSERT_EQ(al.getActivationKey(), activation_key);
    ASSERT_EQ(al.getIntermissionDuration(), true_intermission_duration);
    ASSERT_EQ(al.getCyclesPreintermission(), cycles_preintermission);
    ASSERT_EQ(al.getRepetitions(), repetitions);
    ASSERT_EQ(al.getCharDuration(), char_duration);
}

TEST(Alarm, construcRandomDefault)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = u32_distr(gen);
    const uint32_t beep_duration = u32_distr(gen);
    const char     activation_key = ch_distr(gen);

    const uint32_t true_beep_period = std::max(beep_period, beep_duration);

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    ASSERT_EQ(al.getPriority(), prio);
    ASSERT_EQ(al.getBeepPeriod(), true_beep_period);
    ASSERT_EQ(al.getBeepDuration(), beep_duration);
    ASSERT_EQ(al.getActivationKey(), activation_key);
    ASSERT_EQ(al.getIntermissionDuration(), 0);
    ASSERT_EQ(al.getCyclesPreintermission(), 0);
    ASSERT_EQ(al.getRepetitions(), 0);
    ASSERT_EQ(al.getCharDuration(), 250);
}

TEST(Alarm, activation)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = u32_distr(gen);
    const uint32_t beep_duration = u32_distr(gen);
    const char     activation_key = ch_distr(gen);

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    ASSERT_FALSE(al.isActive()); // should start deactivated

    al.setActive();
    ASSERT_TRUE(al.isActive());

    al.setActive(false);
    ASSERT_FALSE(al.isActive());

}

TEST(Alarm, setCharDur)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = u32_distr(gen);
    const uint32_t beep_duration = u32_distr(gen);
    const char     activation_key = ch_distr(gen);

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    const uint16_t c_dur(u16_distr(gen));

    al.setCharDur(c_dur);
    ASSERT_EQ(al.getCharDuration(), c_dur);
}

TEST(Alarm, getColor)
{
    // highest priority alarm is RED

    const uint16_t prio = Alarm::highest_priority;
    const uint32_t beep_period = u32_distr(gen);
    const uint32_t beep_duration = u32_distr(gen);
    const char     activation_key = ch_distr(gen);

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    ASSERT_EQ(al.getColor(), "\033[1;91m");
}

TEST(Alarm, getSound)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = std::max(uint32_t(1), u32_distr(gen));
    const uint32_t beep_duration = std::max(uint32_t(1), u32_distr(gen));
    const char     activation_key = 'h';

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    std::string colored_beep (al.getColor() + "X\033[0m");

    ASSERT_EQ(al.getSound(), "_"); // getting sound while deactivated should yield "_" (without any color)
    al.setActive();
    ASSERT_EQ(al.getSound(), colored_beep); // getting sound right after activating the alarm should yield "X"
}

TEST(Alarm, getSoundNullAlarm)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = 0;
    const uint32_t beep_duration = 0;
    const char     activation_key = 'h';

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    ASSERT_EQ(al.getSound(), "_"); // getting sound while deactivated should yield "_" (without any color)
    al.setActive();
    ASSERT_EQ(al.getSound(), "_"); // getting sound of silent alarm (even activated) should yield "_" too
}

TEST(Alarm, getSoundDelay)
{
    const uint16_t prio = u16_distr(gen);
    const uint32_t beep_period = 500;
    const uint32_t beep_duration = 250;
    const char     activation_key = 'h';

    Alarm al(prio,
             beep_period,
             beep_duration,
             activation_key);

    std::string colored_silence (al.getColor() + "_\033[0m");
    std::string colored_beep (al.getColor() + "X\033[0m");

    ASSERT_EQ(al.getSound(), "_"); // getting sound while deactivated should yield "_" (without any color)

    auto next_time_pt = std::chrono::steady_clock::now();
    al.setActive();

    ASSERT_EQ(al.getSound(), colored_beep); // getting sound right after activating the alarm should yield "X"

    next_time_pt += std::chrono::milliseconds(beep_duration + (int64_t(beep_period) - beep_duration)/10);
    std::this_thread::sleep_until(next_time_pt);

    ASSERT_EQ(al.getSound(), colored_silence); // getting sound after the above delay should yield "_"
}
