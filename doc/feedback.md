Feedback sur ton alarm player :) Voici mes notes.

- Code propre, très commenté 

- Code build direct, les TU passent et l'appli fonctionne comme attendue.

- Readme nickel +++ 

- API au plus simple pour l'utilisateur,  

- Prises en compte de plusieurs utilisations possibles avec exemples 

- Code alarm.cc assez complexe et difficile à lire, fonctions un peu grosses (Alarm::getSound()) 

- Un singleton sur alarm manager aurait été préférable , nous ne voulons probablement pas 2 systèmes d'alarmes en parallèle sur un unique appareil.

- Utilisation de kbhit buffering pour éviter un thread supplémentaire pour le keyboard... Du coup pas de mutex c'est cool.

- Alarm manager possède les alarmes -> l'architecture est saine

- Des fonctions sont implémentées mais ne sont pas forcément nécessaires (dans alarm.h même si pas visible depuis le code client) … Pour les tests ? Alors private et friend est une alternative pour que ce soit clair pour l'utilisateur ce qu'il doit utiliser ou pas  ("getCharDuration, getRepetitions, getCyclesPreintermission, ... ")


C'est un beau projet, bien réalisé. Nous debriefons en ce moment avec l'équipe car nous avons eus 2 bons projets... Il faut vraiment que tu vois Fabien et Vincent, alors essaie de répondre assez vite à mon mail d'hier stp (le 26/11 à 17h30 en video call ?)
