# Project proposal

The purpose of this exercise is to propose an API and an implementation to play different alarm audio sequences. In material devices, there are 3 different level of alarms : LOW, MEDIUM and HIGH priority.

The audio sequences are as follow:

- LOW: one beep every 30s, with a beep duration of 1s. Repeat continuously
- MEDIUM: one beep every second, with a beep duration of 250ms. Repeat continuously
- HIGH: five beeps every 500ms with duration of 250ms each, then wait for 2s with no beep. Repeat continuously



There are different rules regarding the alarms management:

- There can be only one alarm sequence at a time being played
- Only the highest priority active alarm is played at a time
- They can be started and stopped

If a higher priority alarm is stopped, the lower one might be played if active.

What you need to do:

- You need to provide an API for the client application to start / stop alarms. The simpler, the better.
- You need to provide an appropriate architecture (classes, threads, ...). It has to be robust (we're talking about alarms here), as simple as possible and readable.
- Implement the code WITH unit tests to show that the code is working.

Source code architecture shall be 'cmake' based and should allow us to run the main application as well as run the unit tests.

The application output:

- When application is executed, the output should be continuously displaying characters simulating the buzzer state. One character represents 250ms, '\_' is no beep and 'X is beep'.
- Pressing on 'h': activates/deactivates high priority alarm
- Pressing on 'm': activates/deactivates medium priority alarm
- Pressing on 'l': activates/deactivates low priority alarm
