// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2020, José Mendes Filho
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#ifndef __ALARM_H__
#define __ALARM_H__

#include <stdint.h>  // uint16_t, uint32_t
#include <memory>    // shared_ptr
#include <chrono>
#include <string>

/*! \class Alarm
 *  \brief Provide a generalized way to create alarms
 */
class Alarm
{

public:

    static const uint16_t highest_priority; ///< value of the highest possible priority

    /*! \brief Constructor for Alarm
     *  \param priority: priority of this alarm
     *  \param beep_period: Beeps are periodic, this is the period
     *  \param beep_duration: Time duration of a single beep
     *  \param activation_key: key to toggle alarm
     *  \param intermission_duration: Duration with no beep after cycles_preintermission beeping cycles
     *  \param cycles_preintermission: Number of cycles befor intermission
     *  \param repetitions: Maximum number of beeping cycles
     *  \param char_duration: Time duration of an output character representing silence of beep
     */
    Alarm(uint16_t priority, uint32_t beep_period, uint32_t beep_duration,
          char activation_key, uint32_t intermission_duration = 0,
          uint16_t cycles_preintermission = 0, uint32_t repetitions = 0,
          uint16_t char_duration = 250);

    uint16_t getPriority()              const { return priority_; };
    uint32_t getBeepPeriod()            const { return beep_period_; };
    uint32_t getBeepDuration()          const { return beep_duration_; };
    uint32_t getIntermissionDuration()  const { return intermission_duration_; };
    uint16_t getCyclesPreintermission() const { return cycles_preintermission_; };
    uint32_t getRepetitions()           const { return repetitions_; };
    uint16_t getCharDuration()          const { return char_duration_; };
    char     getActivationKey()         const { return activation_key_; };
    const std::string& getColor()       const { return color_; };

    bool isActive() const { return is_active_; };

    void setCharDur(uint16_t cd) { char_duration_ = cd; };

    /*! \brief Return the sound character ('_'  or 'X') to be played right now
    */
    std::string getSound();

    void setActive(bool active = true);

private:

    const uint16_t priority_;
    const uint32_t beep_period_;
    const uint32_t beep_duration_;
    const uint32_t intermission_duration_;
    const uint16_t cycles_preintermission_;
    const uint32_t repetitions_;

    const char activation_key_;
    const std::string color_;

    uint32_t time_since_start_of_intermission_;

    bool is_active_;
    bool act_rising_edge_;
    uint32_t nb_cycles_since_act_;

    std::chrono::time_point<std::chrono::steady_clock> activation_time_pt_;

    uint16_t char_duration_;
    uint32_t total_intermission_duration_;
};

typedef std::shared_ptr<Alarm> AlarmPtr;

struct CompareAlarms
{
    bool operator()(AlarmPtr a, AlarmPtr b)
    {
        if (a->isActive() == b->isActive()) // if both are activated or deactivated at the same time, only priority matters
        {
            return (a->getPriority() < b->getPriority());
        }
        else if (a->isActive() && !b->isActive()) // an activated alarm comes before an deactivated one regardeless of priority
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};

#endif /* __ALARM_H__ */
