// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2020, José Mendes Filho
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#ifndef __ALARM_MANAGER_H__
#define __ALARM_MANAGER_H__

#include "alarm.h"
#include <signal.h>  // sigaction
#include <vector>
#include <termios.h>
#include <iostream>
#include <sstream>

static bool interrupted;

/*! \class AlarmManager
 *  \brief Encapsulates the main loop which plays the alarms and process the user input
 */
class AlarmManager
{

public:

    /*! \brief Constructor for AlarmManager
     *  \param char_duration: Time duration of an output character representing silence of beep
     */
    AlarmManager(uint16_t char_duration = 250, std::stringstream* output = nullptr);

    /*! \brief Main loop
     */
    void run();

    /*! \brief Stop main loop
     */
    void stop()
    {
        stopped_ = true;
    };

    /*! \brief Add an alarm to the alarm manager
     *  \param alarm: An alarm pointer
     */
    void add(AlarmPtr alarm);

    /*! \brief Add an alarm to the alarm manager
     *  \param beep_period: Beeps are periodic, this is the period
     *  \param beep_duration: Time duration of a single beep
     *  \param activation_key: key to toggle alarm
     *  \param intermission_duration: Duration with no beep after cycles_preintermission beeping cycles
     *  \param cycles_preintermission: Number of cycles befor intermission
     *  \param repetitions: Maximum number of beeping cycles
     */
    void add(uint32_t beep_period, uint32_t beep_duration,
        char activation_key, uint32_t intermission_duration = 0,
        uint16_t cycles_preintermission = 0, uint32_t repetitions = 0);

    /*! \brief Add an alarm to the alarm manager
     *  \param priority: priority of this alarm
     *  \param beep_period: Beeps are periodic, this is the period
     *  \param beep_duration: Time duration of a single beep
     *  \param activation_key: key to toggle alarm
     *  \param intermission_duration: Duration with no beep after cycles_preintermission beeping cycles
     *  \param cycles_preintermission: Number of cycles befor intermission
     *  \param repetitions: Maximum number of beeping cycles
     */
    void add(uint16_t priority, uint32_t beep_period, uint32_t beep_duration,
        char activation_key, uint32_t intermission_duration = 0,
        uint16_t cycles_preintermission = 0, uint32_t repetitions = 0);

    /*! \brief Return total number of added alarms
     */
    size_t nbAlarms() const { return alarms_.size(); };

    /*! \brief Return total number of added active alarms
     */
    size_t nbActiveAlarms() const
    {
        size_t nb_active = 0;
        for (const auto& alarm : alarms_)
        {
            if (alarm->isActive())
            {
                nb_active++;
            }
        }
        return nb_active;
    }

private:

    std::vector<AlarmPtr> alarms_;
    CompareAlarms compare_alarms_;

    std::ostream out_;
    std::stringstream* ss_;

    uint16_t char_duration_;

    bool stopped_;

    uint16_t next_prio_; ///< keep tabs on the added alarm with lowest priority (useful for add method without priority arg)

    struct sigaction sigIntHandler_; ///< handle ctrl-c

    static void signalHandler_(int s)
    {
        // Signals to be ignored
        if (s == SIGWINCH || s == SIGURG || s == SIGCHLD)
            return;
        interrupted = true;
    };

    termios initial_term_settings_;

    void processInput_();

    int kbhit_();

};

#endif /* __ALARM_MANAGER_H__ */
