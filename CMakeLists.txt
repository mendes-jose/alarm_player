cmake_minimum_required(VERSION 3.10)
project(alarm_player)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_BUILD_TYPE Release) #None, Debug, Release, ExtremeDebug

# find_package(Curses REQUIRED)

include_directories(include)
# include_directories(include ${CURSES_INCLUDE_DIR})
file(GLOB SRC_FILES ${PROJECT_SOURCE_DIR}/src/*.cpp) # get all cpp
list(FILTER SRC_FILES EXCLUDE REGEX ".*main\.cpp$") # exclude main cpp

add_library(${PROJECT_NAME}_lib ${SRC_FILES})
add_executable(${PROJECT_NAME} src/main.cpp)
target_link_libraries(${PROJECT_NAME}
    ${PROJECT_NAME}_lib
)
# target_link_libraries(${PROJECT_NAME} ${CURSES_LIBRARIES})


###########
#  Tests  #
###########

if (USE_GTEST)
    enable_testing()
    include(GoogleTest)

    file(GLOB TEST_SRC_FILES ${PROJECT_SOURCE_DIR}/test/*_test.cpp) # get all test cpp

    add_executable(${PROJECT_NAME}-tests ${TEST_SRC_FILES})
    target_link_libraries(${PROJECT_NAME}-tests gtest pthread ${PROJECT_NAME}_lib)
endif()
